cmake_minimum_required(VERSION 3.17)
project(Course)

set(CMAKE_CXX_STANDARD 20)

add_executable(Course main.cpp test_runner.h condition_parser.cpp condition_parser.h condition_parser_test.cpp
        database.cpp database.h database_test.cpp database_test.cpp date.cpp date.h date_test.cpp event_set.cpp
        event_set.h node.cpp node.h node_test.cpp token.cpp token.h)